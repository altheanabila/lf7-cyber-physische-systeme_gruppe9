import serial
from datetime import datetime
import mariadb
import sys

# Functions
def connect_to_database(): #connect to databae
    try:
        conn = mariadb.connect(
            user="lf7",
            password="lf7",
            host="localhost",
            port=3306,
            database="lf7"
        )
        return conn
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)

def insert_motion_record(conn, room_id, motionStartDate, motionTimeStart, motionTimeEnd, duration): #insert data statement
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO DetectedMotions (room_id, motionStartDate, motionTimeStart, motionTimeEnd, duration) VALUES (?, ?, ?, ?, ?)",
                    (room_id, motionStartDate, motionTimeStart, motionTimeEnd, duration.total_seconds()))
    except mariadb.Error as e:
        print(f"Error: {e}")
    conn.commit()
    cur.close()

def main():
    arduino_port = '/dev/ttyACM0'  # Remember to change the address
    baud_rate = 9600

    motion_start_times = {}  # Dictionary to store motion start times

    conn = connect_to_database()
    arduino = serial.Serial(arduino_port, baud_rate, timeout=1)

    print(f"Connected to {arduino_port} at {baud_rate} baud.")

    try:
        while True:
            line = arduino.readline().decode('utf-8').strip()  # Read data from Arduino

            if line.startswith("motionStart"):
                sensor_id = int(line.split("motionStart")[1])  # Extract sensor ID
                motion_start_times[sensor_id] = datetime.now()  # Store start time
                motionStartDate = motion_start_times[sensor_id].strftime("%Y-%m-%d")  # Format date
                motionTimeStart = motion_start_times[sensor_id].strftime("%H:%M:%S")  # Format time
                print(f"Motion detector {sensor_id} started at date: {motionStartDate}")
                print(f"Motion started at time: {motionTimeStart}")
                print("\n")

            if line.startswith("motionEnd"):
                sensor_id = int(line.split("motionEnd")[1])  # sensor ID
                if sensor_id in motion_start_times:
                    motion_end_time = datetime.now()  # Get end time
                    duration = motion_end_time - motion_start_times[sensor_id]  # Calculate duration
                    motionTimeEnd = motion_end_time.strftime("%H:%M:%S")  # Format end time

                    print(f"Motion detector {sensor_id} ended at time: {motionTimeEnd}")
                    print(f"Motion duration: {duration}")
                    insert_motion_record(conn, sensor_id, motionStartDate, motionTimeStart, motionTimeEnd, duration) # insert data into database
                    del motion_start_times[sensor_id]  # Remove  entry from the dictionary
                    print("\n", "\n")

    except serial.SerialException as e:
        print(f"Error: {e}")
    except KeyboardInterrupt:
        print("Disconnected from Arduino.")
    finally:
        arduino.close()
        conn.close()

if __name__ == "__main__":
    main()
