const int MOTION_SENSOR_PINS[] = {7, 8, 9};  
const int LED_PINS[] = {2, 4, 6};            
const int NUM_SETUPS = 2;                 
int motionStateCurrent[NUM_SETUPS];      
int motionStatePrevious[NUM_SETUPS];     
unsigned long motionStartTime[NUM_SETUPS]; 
const unsigned long motionDuration = 10000; 
bool motionDetected[NUM_SETUPS];

bool initialLightState[NUM_SETUPS] = {false, false};  

void setup() {
  Serial.begin(9600);  
  
  for (int i = 0; i < NUM_SETUPS; i++) {
    pinMode(MOTION_SENSOR_PINS[i], INPUT); 
    pinMode(LED_PINS[i], OUTPUT);          
    digitalWrite(LED_PINS[i], initialLightState[i]);  
  }
}

void loop() {
  for (int i = 0; i < NUM_SETUPS; i++) {
    motionStatePrevious[i] = motionStateCurrent[i];            
    motionStateCurrent[i] = digitalRead(MOTION_SENSOR_PINS[i]);  

    if (motionStateCurrent[i] == HIGH) { 
      if (!motionDetected[i]) {
        Serial.print("motionStart");
        Serial.println(i + 1); 
      motionDetected[i] = true;
      motionStartTime[i] = millis(); 
      digitalWrite(LED_PINS[i], HIGH); 
    }

    if (motionDetected[i] && millis() - motionStartTime[i] >= motionDuration) { 
      digitalWrite(LED_PINS[i], LOW); 
      Serial.print("motionEnd");
      Serial.println(i + 1); 
      motionDetected[i] = false;
    }
  }
}

