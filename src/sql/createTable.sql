/* Rooms */
CREATE TABLE Classrooms (
    room_id INT AUTO_INCREMENT PRIMARY KEY,
    room_number INT
);

/*insert rooms*/
INSERT INTO Classrooms VALUES (room_number, 103), (room_number, 104);

/* Detected motions */
CREATE TABLE DetectedMotions (
    motion_id INT AUTO_INCREMENT PRIMARY KEY,
    room_id INT,
    motionStartDate DATE,
    motionTimeStart TIME,
    motionTimeEnd TIME,
    duration char(50), 
    FOREIGN KEY (room_id) REFERENCES Classrooms(room_id)
);

