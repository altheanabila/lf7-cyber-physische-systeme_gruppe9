import mariadb
import matplotlib.pyplot as plt

conn = mariadb.connect(
    user="lf7",
    password="lf7",
    host="localhost",
    database="lf7")
cur = conn.cursor()

query = "SELECT CR.room_number, SUM(DM.duration) FROM DetectedMotions DM INNER JOIN Classrooms CR ON DM.room_id =  CR.room_id GROUP BY DM.room_id"

cur.execute(query)
results = cur.fetchall()

cur.close()
conn.close()

room_number, sums = zip(*results)
plt.xticks(room_number)

plt.bar(room_number, sums)
plt.xlabel('room_number')
plt.ylabel('Sum')
plt.show()